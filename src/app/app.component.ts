import { Component } from '@angular/core';

@Component({
  selector: 'pozkfe-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pozkfe';
}
