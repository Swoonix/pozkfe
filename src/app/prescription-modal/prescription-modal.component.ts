import { PatientService } from './../services/patient.service';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSelectChange } from '@angular/material';

export interface Medication {
  value: string;
  code: string;
}

export interface Genre {
  value: string;
  viewValue: string;
}

export interface Allergie {
  value: string;
}

export interface MedicationRequest {
  idPatient: string;
  medication: string;
}

export interface Period {
  period: string;
  periodUnit: string;
  viewValue: string;
}

@Component({
  selector: 'pozkfe-prescription-modal',
  templateUrl: './prescription-modal.component.html',
  styleUrls: ['./prescription-modal.component.scss']
})

export class PrescriptionModalComponent {

  allergique: boolean;
  medicationRequest: MedicationRequest;
  selectedMed: Medication;
  disabled: boolean;
  nbAllergies: number;

  medicaments: Medication[] = [
    { value: 'Amoxicilline', code: '91936005' },
    { value: 'Doliprane', code: '9999999' },
    { value: 'Efferalgan', code: '9828828' }
  ];

  sexes: Genre[] = [
    { value: 'male', viewValue: 'Homme' },
    { value: 'female', viewValue: 'Femme' },
  ];

  unite = [
    'mg', 'g', 'mL', 'L'
  ];

  modes = [
    'comprimé', 'cuillère à soupe', 'injection'
  ];

  temps: Period[] = [
    {period: '1', periodUnit: 'd', viewValue: 'par jour'},
    {period: '1', periodUnit: 'wk', viewValue: 'par semaine'},
    {period: '1', periodUnit: 'mo', viewValue: 'par mois'},
    {period: '1', periodUnit: 'y', viewValue: 'par an'}
  ];

  selectedPeriod: Period;

  constructor(
    public dialogRef: MatDialogRef<PrescriptionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private patientService: PatientService) { console.log(data); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // tslint:disable-next-line:member-ordering
  formPrescription = new FormGroup({
    medication: new FormControl('', [Validators.required]),
    familyName: new FormControl('', [Validators.required]),
    givenName: new FormControl('', [Validators.required]),
    gender: new FormControl('', [Validators.required]),
    dosage: new FormControl('', [Validators.required]),
    dosageUnit: new FormControl('', [Validators.required]),
    rateQty: new FormControl('', [Validators.required]),
    route: new FormControl('', [Validators.required]),
    repeat: new FormControl('', [Validators.required])
  });

  async send() {
    // tslint:disable-next-line:no-string-literal
    console.log(this.formPrescription.value);
    await this.patientService.postMedicationRequest(this.data.patient.id, this.formPrescription.value, this.selectedPeriod);
    this.dialogRef.close();
  }

  checkAllergie(event: MatSelectChange) {
    this.allergique = false;
    this.nbAllergies = 0;
    if (this.data.patient.allergies) {

      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.medicaments.length; i++) {
        if (this.medicaments[i].value === event.value) {
          this.selectedMed = this.medicaments[i];
        }
      }

      if (this.selectedMed) {
        for (let i = 0; i <= this.data.patient.codeAllergies.length; i++) {
          if (this.selectedMed.code === this.data.patient.codeAllergies[i]) {
            this.allergique = true;
            this.disabled = true;
            this.nbAllergies++;
          }
        }
      }

      if (this.nbAllergies === 0) {
        this.allergique = false;
        this.disabled = false;
      }

    }
  }

  setSelectedPeriod(event: MatSelectChange) {
    console.log(event);
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.temps.length; i++) {
      if (this.temps[i].viewValue === event.value) {
        this.selectedPeriod = this.temps[i];
        console.log(this.selectedPeriod);
      }
    }
  }
}
