import { PatientService } from './../services/patient.service';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'pozkfe-ordonnance-modal',
  templateUrl: './ordonnance-modal.component.html',
  styleUrls: ['./ordonnance-modal.component.scss']
})
export class OrdonnanceModalComponent {

  constructor(
    public dialogRef: MatDialogRef<OrdonnanceModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private patientService: PatientService) { console.log(data); }




}
