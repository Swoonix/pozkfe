import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdonnanceModalComponent } from './ordonnance-modal.component';

describe('OrdonnanceModalComponent', () => {
  let component: OrdonnanceModalComponent;
  let fixture: ComponentFixture<OrdonnanceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdonnanceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdonnanceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
