import { MedicationRequest } from './../prescription-modal/prescription-modal.component';
import { map, filter } from 'rxjs/operators';
// tslint:disable:max-line-length
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PatientService {

    constructor(private http: HttpClient) { }

    /**
     * Retrieve all patients
     */
    getPatients(): Observable<any> {
        return this.http.get('https://fhir.eole-consulting.io/api/patient/');
    }

    getDoctor(): Observable<any> {
        return this.http.get('https://fhir.eole-consulting.io/api/practitioner/5d7f96a032364000151f8ac0');
    }

    getOrdonnances(): Observable<any> {
        return this.http.get('https://fhir.eole-consulting.io/api/medication-request/');
    }

    // getIntelorences(id: string): any {
    //     return this.http.get('https://fhir.eole-consulting.io/allergy-intolerance/').pipe(filter(intolerance => intolerance.patient === `Patient/${id}'
    //     ));
    // }

    postMedicationRequest(id: string, formGroup: any, selectedPeriod: any) {
        console.log(formGroup);
        console.log(selectedPeriod);

        const text = formGroup.medication + ' ' + formGroup.dosage + ' ' + formGroup.dosageUnit + ' : ' + selectedPeriod.period + ' ' + formGroup.route + ' ' + selectedPeriod.viewValue;

        const payload = JSON.parse(`{
            "resourceType": "MedicationRequest",
            "status": "active",
            "intent": "order",
            "medicationReference": {
                "reference": "#med0304",
                "display": "${formGroup.medication}"
            },
            "subject": {
                "reference": "Patient/${id}"
            },
            "authoredOn": "${new Date()}",
            "recorder": {
                "reference": "Practitioner/5d7f96a032364000151f8ac0"
            },
            "dosageInstruction": [
                {
                    "timing": {
                        "repeat": {
                            "period": ${selectedPeriod.period},
                            "periodUnit": "${selectedPeriod.periodUnit}"
                        }
                    },
                    "route": {
                        "coding": [
                            {
                                "display": "${formGroup.route}"
                            }
                        ]
                    },
                    "text": "${text}",
                    "doseQuantity": {
                        "value": ${formGroup.dosage},
                        "unit": "${formGroup.dosageUnit}",
                        "system": "http://unitsofmeasure.org",
                        "code": "${formGroup.dosageUnit}"
                       }
                }
            ]
        }`);
        console.log(payload);
        return this.http.post('https://fhir.eole-consulting.io/api/medication-request/', payload).toPromise();
    }
}
