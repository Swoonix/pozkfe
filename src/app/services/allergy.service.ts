import { map, filter } from 'rxjs/operators';
// tslint:disable:max-line-length
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AllergyService {

    constructor(private http: HttpClient) { }

    /**
     * Retrieve all patients
     */
    getAllergies(): Observable<any> {
        return this.http.get('https://fhir.eole-consulting.io/api/allergy-intolerance/');
    }

    // getIntelorences(id: string): any {
    //     return this.http.get('https://fhir.eole-consulting.io/allergy-intolerance/').pipe(filter(intolerance => intolerance.patient === `Patient/${id}'
    //     ));
    // }
}
