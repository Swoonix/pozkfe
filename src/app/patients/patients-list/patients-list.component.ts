
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {PrescriptionModalComponent} from 'src/app/prescription-modal/prescription-modal.component';
import { OrdonnanceModalComponent } from 'src/app/ordonnance-modal/ordonnance-modal.component';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'pozkfe-patients-list',
  templateUrl: './patients-list.component.html',
  styleUrls: ['./patients-list.component.scss']
})
export class PatientsListComponent implements OnInit {
  @Input() patient: any;
  @Input() doctor: any;
  @Output() update: EventEmitter<any> = new EventEmitter<any>();

  initials: string;
  colors = ['#eb5757', '#f2994a', '#2f80ed', '#219653', '#9b51e0'];
  color = '';
  patientAge: number;
  gender = '';

  constructor(public dialog: MatDialog, private snackBar: MatSnackBar) { }

  ngOnInit() {
    if (this.patient.name) {
      this.getInitials();
      this.setRandomColor();
      this.calcAge();
      this.translateGender();
    }
  }


  getInitials() {
    const surnameInitial = this.patient.name[0].given[0].substring(0, 1);
    const nameInitial = this.patient.name[0].family.substring(0, 1);
    this.initials = '' + surnameInitial + nameInitial;
  }

  setRandomColor() {
    this.color = this.colors[Math.floor(Math.random() * this.colors.length)];
  }

  calcAge() {
    const birthday = +new Date(this.patient.birthDate);
    // tslint:disable-next-line:no-bitwise
    this.patientAge = ~~((Date.now() - birthday) / (31557600000));
  }

  translateGender() {
    this.patient.gender === 'male' ? this.gender = 'homme' : this.gender = 'femme';
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(PrescriptionModalComponent, {
      width: '650px',
      data: {patient: this.patient}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.openSnackBar('Ordonnance créée !', 'Fermer');
      this.update.emit('update');
    });

  }

  openOrdonnance(id): void {
    let ordo;

    for (const ord of this.patient.ordonnances) {
      if (id === ord.id) {
        ordo = ord;
      }
    }
    const dialogRef = this.dialog.open(OrdonnanceModalComponent, {
      width: '450px',
      data: {
        patient: this.patient,
        ordonnance: ordo,
        doctor: this.doctor
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 20000,
    });
  }

}
