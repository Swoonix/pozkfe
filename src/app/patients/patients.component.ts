import { Component, OnInit, OnDestroy } from '@angular/core';
import { PatientService } from './../services/patient.service';
import { AllergyService } from './../services/allergy.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'pozkfe-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  patients: any = [];
  ourPatient: any = [];
  allergies: any = [];
  ordonnances: any = [];
  doctor: any;
  search: string;

  id = 'Practitioner/5d7f96a032364000151f8ac0';
  constructor(private patientService: PatientService, private allergyService: AllergyService) { }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    for (const sub of this.subs) {
      sub.unsubscribe();
    }
    this.subs = [];
  }

  loadData() {
    this.patients = [];
    this.allergies = [];
    this.ordonnances = [];
    this.subs.push(this.patientService.getPatients().subscribe(result => {
      this.patients = result;
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.patients.length; i++) {
        if (this.patients[i].id === '5d7fa69332364000151f8ac3') {
          this.ourPatient = this.patients[i];
        }
      }
    }));

    this.subs.push(this.allergyService.getAllergies().subscribe(result => {
      this.allergies = result;
      this.setAllergies();
    }));

    this.subs.push(this.patientService.getOrdonnances().subscribe(result => {
      for (const ordonnance of result) {
        if (ordonnance.recorder) {
          if (ordonnance.recorder.reference === this.id ) {
            this.ordonnances.push(ordonnance);
          }
        }

      }
      this.setOrdonnances();
      console.log(this.patients);
    }));

    this.subs.push(this.patientService.getDoctor().subscribe(result => {
      this.doctor = result;
    }));
  }

  setAllergies() {
    // tslint:disable-next-line:prefer-for-of
    for (let j = 0; j < this.patients.length; j++) {
      this.patients[j].allergies = [];
      this.patients[j].codeAllergies = [];
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.allergies.length; i++) {
        if (this.allergies[i].patient.reference === 'Patient/' + this.patients[j].id && this.allergies[i].code) {
          this.patients[j].allergies.push(this.allergies[i].code.coding[0].display);
          if (this.allergies[i].code.coding[0].code) {
            this.patients[j].codeAllergies.push(this.allergies[i].code.coding[0].code);
          }
        }
      }
    }
  }

  setOrdonnances() {
    // tslint:disable-next-line:prefer-for-of
    for (let j = 0; j < this.patients.length; j++) {
      this.patients[j].ordonnances = [];
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.ordonnances.length; i++) {
        if (this.ordonnances[i].subject.reference === 'Patient/' + this.patients[j].id) {
          this.patients[j].ordonnances.push(this.ordonnances[i]);
        }
      }
    }
  }

  checkContains(patient) {
    if (this.search) {
      if (patient.name[0].given[0].toUpperCase().includes(this.search.toUpperCase()) ||
      patient.name[0].family.toUpperCase().includes(this.search.toUpperCase())) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  onUpdate(event) {
    this.loadData();
  }

}
